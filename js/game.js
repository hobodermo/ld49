function Game() {
    this.level = 0;
    this.customPieces = 5;
    this.turns = 120;
    this.currentOption = "";
    this.isContextMenuOpen = false;
    this.randomOption = "";
    this.$profileBar = generateProfileBar();
    this.map = generateMap();
    this.$game = $("<div>").attr("id", "game");
    this.$game.append(this.$profileBar);
    this.$game.append(this.map.$map);
    this.$contextMenu = generateContextMenu();
    this.$game.append(this.$contextMenu);
    this.$profileBar.css("width", (44 * this.map.size.width) + "px");
    this.$profileBar.find("#customPiecesRemaining").text("Custom selections left: " + this.customPieces);
    this.$profileBar.find("#turnsRemaining").text("Turns remaining until valves open: " + this.turns);
    loadEntryPoints(this.map);
    this.$game.on("contextmenu", contextMenuOpen.bind(this))
              .on("click", gameClick.bind(this));

    this.$profileBar.on("click", "#randomSelection", randomButtonClick.bind(this));
    this.$contextMenu.on("click", ".contextMenuRow", contextMenuClick.bind(this));

    this.timer = null;

    function randomButtonClick() {
        var options = [
            "cross",
            "junction",
            "junction rotate90",
            "junction rotate180",
            "junction rotate270",
            "turn",
            "turn rotate90",
            "turn rotate180",
            "turn rotate270",
            "pipe",
            "pipe rotate90"
        ];
        if(this.randomOption == "") {
            this.randomOption = options[Utilities.RandomInt(0, options.length-1)];
        }
        this.currentOption = this.randomOption;
        this.$profileBar.find("div.cell").removeClass("cross junction pipe turn rotate90 rotate180 rotate270")
            .addClass(this.currentOption);
    }

    function generateProfileBar() {
        var $profile = $("<div>").attr("id", "profileBar")
            .append($("<div>").addClass("cell"))
            .append($("<button>").attr("id", "randomSelection").text("Random"))
            .append($("<span>").attr("id", "customPiecesRemaining"))
            .append($("<span>").attr("id", "turnsRemaining"));
        return $profile;
    }

    function loadEntryPoints(map) {
        var firstInputIndex = Utilities.RandomInt(1, map.size.width) - 1;
        map.entryPoints = [firstInputIndex];
        map.rows[5][firstInputIndex].$cell.addClass("pipe");
        map.rows[6][firstInputIndex].$cell.addClass("pipe");
        map.rows[6][firstInputIndex].entryPoint = true;

        var secondInputIndex = Utilities.RandomInt(1, map.size.width) - 1;
        while(secondInputIndex == firstInputIndex) {
            secondInputIndex = Utilities.RandomInt(1, map.size.width) - 1;
        }
        map.entryPoints.push(secondInputIndex);
        map.rows[5][secondInputIndex].$cell.addClass("pipe");
        map.rows[6][secondInputIndex].$cell.addClass("pipe");
        map.rows[6][secondInputIndex].entryPoint = true;
    }

    function contextMenuClick(e) {
        e.stopPropagation();
        this.currentOption = $(e.target).attr("data-option");
        this.$profileBar.find("div.cell").removeClass("cross junction pipe turn rotate90 rotate180 rotate270")
            .addClass(this.currentOption);
        this.isContextMenuOpen = false;
        this.$contextMenu.hide();
    }

    function contextMenuOpen(e) {
        e.preventDefault();
        if(this.customPieces > 0) {
            this.isContextMenuOpen = true;
            this.$contextMenu.css({"top": e.pageY + "px", "left": e.pageX + "px"}).show();
        }
    }

    function gameClick(e) {
        if(this.isContextMenuOpen) {
            this.isContextMenuOpen = false;
            this.$contextMenu.hide();
        } else {
            var $cell = $(e.target);
            if($cell.parent().hasClass("underground") && !(this.currentOption == null || this.currentOption == "")) {
                //Valid game step
                $cell.removeClass("cross junction pipe turn rotate90 rotate180 rotate270").addClass(this.currentOption);
                if(this.currentOption != this.randomOption) {
                    this.customPieces--;
                    this.$profileBar.find("#customPiecesRemaining").text("Custom selections left: " + this.customPieces);
                }
                if(this.currentOption == this.randomOption || this.customPieces == 0) {
                    this.randomOption = "";
                    randomButtonClick.call(this);
                    this.currentOption = this.randomOption;
                        this.$profileBar.find("div.cell").removeClass("cross junction pipe turn rotate90 rotate180 rotate270")
                        .addClass(this.currentOption);
                }
            }
        }
    }

    function generateMap() {
        var i, j, $cell, $row, $tempCell, $tempRow, rowBackgroundClass, row, map = {
            size: {
                height: 20,
                width: 30
            },
            rows: [],
            $map: null,
            flooded: []
        };
        $cell = $("<div>").addClass("cell");
        $row = $("<div>").addClass("row").css("width", (44 * map.size.width) + "px");
        map.$map = $("<div>").attr("id", "map").css({"width": (44 * map.size.width) + "px", "height": (44 * map.size.height) + "px"});

        for(i=0; i<map.size.height; i++) {
            rowBackgroundClass = generateRowBackgroundClass(i);
            $tempRow = $row.clone().attr("data-row", i).addClass(rowBackgroundClass);
            row = [];
            for(j=0; j<map.size.width; j++) {
                $tempCell = $cell.clone().attr("data-cell", j);
                row.push({
                    position: {
                        row: i,
                        cell: j
                    },
                    $cell: $tempCell,
                    areaType: rowBackgroundClass,
                    flooded: false
                })
                $tempRow.append($tempCell);
            }
            map.rows.push(row);
            map.$map.append($tempRow);
        }

        return map;

        function generateRowBackgroundClass(index) {
            if(index < 2) return "sky";
            if(index < 5) return "ground";
            if(index < 7) return "crust";
            if(index > 17) return "magma";
            return "underground";
        }
    }

    function generateContextMenu() {
        var $clear = $("<div>").addClass("contextMenuRow").attr("data-option", "").text(" Clear")
            .prepend($("<div>").addClass("cell"));
        var $cross = $("<div>").addClass("contextMenuRow").attr("data-option", "cross").text(" Cross")
            .prepend($("<div>").addClass("cell cross"));
        var $junc = $("<div>").addClass("contextMenuRow").attr("data-option", "junction").text(" Junction 1")
            .prepend($("<div>").addClass("cell junction"));
        var $junc90 = $("<div>").addClass("contextMenuRow").attr("data-option", "junction rotate90").text(" Junction 2")
            .prepend($("<div>").addClass("cell junction rotate90"));
        var $junc180 = $("<div>").addClass("contextMenuRow").attr("data-option", "junction rotate180").text(" Junction 3")
            .prepend($("<div>").addClass("cell junction rotate180"));
        var $junc270 = $("<div>").addClass("contextMenuRow").attr("data-option", "junction rotate270").text(" Junction 4")
            .prepend($("<div>").addClass("cell junction rotate270"));
        var $turn = $("<div>").addClass("contextMenuRow").attr("data-option", "turn").text(" Turn 1")
            .prepend($("<div>").addClass("cell turn"));
        var $turn90 = $("<div>").addClass("contextMenuRow").attr("data-option", "turn rotate90").text(" Turn 2")
            .prepend($("<div>").addClass("cell turn rotate90"));
        var $turn180 = $("<div>").addClass("contextMenuRow").attr("data-option", "turn rotate180").text(" Turn 3")
            .prepend($("<div>").addClass("cell turn rotate180"));
        var $turn270 = $("<div>").addClass("contextMenuRow").attr("data-option", "turn rotate270").text(" Turn 4")
            .prepend($("<div>").addClass("cell turn rotate270"));
        var $pipeV = $("<div>").addClass("contextMenuRow").attr("data-option", "pipe").text(" Pipe V")
            .prepend($("<div>").addClass("cell pipe"));
        var $pipeH = $("<div>").addClass("contextMenuRow").attr("data-option", "pipe rotate90").text(" Pipe H")
            .prepend($("<div>").addClass("cell pipe rotate90"));
        return $("<div>").attr("id", "contextMenu").append($clear, $cross, $junc, $junc90, $junc180, $junc270, $turn, $turn90, $turn180, $turn270, $pipeV, $pipeH);
    }
}

Game.prototype.Start = function() {
    $("body").html(this.$game);
    this.timer = setInterval(decreaseTurns.bind(this), 1000);

    function decreaseTurns() {
        this.turns--;
        if(this.turns <= 0) {
            this.IncreaseSewage();
        }
        if(this.turns >= 0) {
            this.$profileBar.find("#turnsRemaining").text("Turns remaining until valves open: " + this.turns);
        }
    }
};

Game.prototype.IncreaseSewage = function() {
    if(this.turns == 0) {
        this.map.entryPoints.forEach(function(val) {
            var cell = this.map.rows[5][val];
            this.map.flooded.push(cell);
            cell.$cell.addClass("shit");
            cell.flooded = true;
        }, this);
        return;
    }
    var flooded = [];
    this.map.flooded.forEach(function(cell, i, arr) {
        var nextCell;
        var output = getOutputs(cell);
        if(output.indexOf("UP") != -1) {
            if(cell.position.row) {
                nextCell = this.map.rows[cell.position.row - 1][cell.position.cell];
                if(!nextCell.flooded && nextCell.areaType == "underground") {
                    flooded.push(nextCell);
                    nextCell.$cell.addClass("shit");
                    nextCell.flooded = true;
                }
            }
        }
        if(output.indexOf("DOWN") != -1) {
            if(cell.position.row + 1 <= this.map.size.height) {
                nextCell = this.map.rows[cell.position.row + 1][cell.position.cell];
                if(!nextCell.flooded && nextCell.areaType != "magma") {
                    flooded.push(nextCell);
                    nextCell.$cell.addClass("shit");
                    nextCell.flooded = true;
                }
            }
        }
        if(output.indexOf("LEFT") != -1) {
            if(cell.position.cell) {
                nextCell = this.map.rows[cell.position.row][cell.position.cell - 1];
                if(!nextCell.flooded) {
                    flooded.push(nextCell);
                    nextCell.$cell.addClass("shit");
                    nextCell.flooded = true;
                }
            }
        }
        if(output.indexOf("RIGHT") != -1) {
            if(cell.position.cell + 1 <= this.map.size.width) {
                nextCell = this.map.rows[cell.position.row][cell.position.cell + 1];
                if(!nextCell.flooded) {
                    flooded.push(nextCell);
                    nextCell.$cell.addClass("shit");
                    nextCell.flooded = true;
                }
            }
        }
    }, this);

    this.map.flooded = flooded;

    if(!this.map.flooded.length && this.turns < 0) {
        this.NextLevel();
    } else {
        if(!this.map.flooded.every(function(cell) {
            return getOutputs(cell).length;
        })) {
            alert("LOSER!");
            clearInterval(this.timer);
        }
    }

    function getOutputs(cell) {
        /*{
         position: {
            row: i,
            cell: j
         },
         $cell: $tempCell,
         areaType: rowBackgroundClass,
         flooded: false
         }*/
        var $cell = cell.$cell;
        if($cell.hasClass("cross")) {
            return ["UP", "DOWN", "LEFT", "RIGHT"];
        }
        if($cell.hasClass("junction")) {
            if($cell.hasClass("rotate90")) {
                return ["UP", "LEFT", "RIGHT"];
            }
            if($cell.hasClass("rotate180")) {
                return ["UP", "DOWN", "RIGHT"];
            }
            if($cell.hasClass("rotate270")) {
                return ["DOWN", "LEFT", "RIGHT"];
            }
            return ["UP", "DOWN", "LEFT"];
        }
        if($cell.hasClass("pipe")) {
            if($cell.hasClass("rotate90")) {
                return ["LEFT", "RIGHT"];
            }
            return ["UP", "DOWN"];
        }
        if($cell.hasClass("turn")) {
            if($cell.hasClass("rotate90")) {
                return ["UP", "RIGHT"];
            }
            if($cell.hasClass("rotate180")) {
                return ["DOWN", "RIGHT"];
            }
            if($cell.hasClass("rotate270")) {
                return ["DOWN", "LEFT"];
            }
            return ["UP", "LEFT"];
        }
        return [];
    }
};

Game.prototype.NextLevel = function() {
    alert("WINNER");
    clearInterval(this.timer);
};