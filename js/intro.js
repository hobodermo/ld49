function Intro(options) {
    //Initialise intro state
    //Draw intro
    this.startGameCallback = options.startGameCallback;
    this.introMessage = "Click here to start the game";
    this.$element = $("<div>").attr("id", "intro");
}

Intro.prototype.Start = function() {
    $("body").html(this.$element);
    this.$element.html(this.introMessage).on("click", this.startGameCallback);
};