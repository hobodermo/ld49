function Main() {
    this.currentState = States[0];
    this.introduction = new Intro({
        startGameCallback: this.StartGame.bind(this)
    });
    this.game = new Game();
}

Main.prototype.Start = function() {
    this.introduction.Start();
    this.currentState = States[1];
};

Main.prototype.StartGame = function() {
    this.game.Start();
    this.currentState = States[2]
}


var States = ["Loading", "Intro", "Game"];

var Instance = new Main();
Instance.Start();